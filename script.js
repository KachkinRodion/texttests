﻿if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
              ? args[number]
              : match
            ;
        });
    };
}

var state =
{
    isTestCompleted: false,
    attemptsLeft: 0,
    answers: [],
    isHintsVisible: false
};


$(createView);


function showHints()
{
    hideHints();
    state.isHintsVisible = true;
    var correct = 0;
    for (var testIndex = 0; testIndex < theTest.tests.length; testIndex++) {
        if (theTest.tests[testIndex].type == "textedit")
        {
            var data = theTest.tests[testIndex].texteditData;
            if (checkTextEdit(data)){
                correct++;
            }
        }
        else if (theTest.tests[testIndex].type == "select")
        {
            var data = theTest.tests[testIndex];
            if (checkSelect(data)) {
                correct++;
            }
        }
        else
        {
            var answers = theTest.tests[testIndex].answers;
            var isCorrect = true;
            for (var answerIndex = 0; answerIndex < answers.length; answerIndex++) {
                var answer = answers[answerIndex];
                if (answer.ui.checked != answer.isCorrect) {
                    isCorrect = false;
                    break;
                }
            }
            if (isCorrect) {
                correct++;
            }

            if (state.attemptsLeft == 0 || state.isTestCompleted) {
                for (var answerIndex = 0; answerIndex < answers.length; answerIndex++) {
                    var answer = answers[answerIndex];

                    if (answer.isCorrect && !answer.ui.checked)
                    {
                        $(answer.ui).parent().addClass("correct");
                    }
                    else if (!answer.isCorrect && answer.ui.checked)
                    {
                        $(answer.ui).parent().addClass("error");
                    }
                }
            }
            else if (!isCorrect)
            {
                var jo = $(answers[0].ui).parent().parent().parent(); 
                jo.addClass("error");
                jo.find('label').addClass("error");
            }
        }
    }


    var resID = 'result1';
    var d = correct % 100;
    if (d % 10 == 1 && d != 11)
    {
        resID = 'result1';
    }
    else if (d % 10 > 1 && d % 10 < 5 && correct != 12 && correct != 13 && correct != 14)
    {
        resID = 'result2_4';
    }
    else
    {
        resID = 'result5_20';
    }

    $("#resultDiv").text(String.format(theTest.resources[resID], correct, theTest.tests.length));


    if (state.attemptsLeft > 0 && !state.isTestCompleted)
        $("#tryAgain").css("visibility", "visible");
    else
        $("#tryAgain").css("visibility", "hidden");
    $("#results").css("visibility", "visible");
}

function hideHints()
{
    $(".correct").removeClass("correct");
    $(".error").removeClass("error");
    state.isHintsVisible = false;
}

function createView()
{
    if (theTest.fontsize !== "undefined")
    {
        $('body').css('font-size', theTest.fontsize + 'px')
    }
    state.attemptsLeft = theTest.attempts;
    var html = String.format('<h1>{0}</h1>', theTest.title);
    var counter = 0;
    for (var i = 0; i < theTest.tests.length; i++)
    {
        var test = theTest.tests[i];
        if (!test.skipNumber)
        {
            html += createTest(test, i, ++counter);
        }
        else
        {
            html += createTest(test, i, "");
        }
    }
    html += String.format('<div class="checkButton checkButtonDisabled" id="checkBtn"></div><div id="results"><div class="result" id="resultDiv"></div><div id="tryAgain">{0}</div>',
      theTest.resources['tryAgain']);
    $("#content").html(html);

    $('input,select').click(function (event) {
        if (state.attemptsLeft > 0 && !state.isTestCompleted)
        {
            var me = $(event.target);
            me.removeClass("error");
            me.removeClass("correct");
            me.parent().parent().find(".error").removeClass("error");
            me.parent().parent().find(".correct").removeClass("correct");
            var parents = me.parents(".test");
            parents.removeClass("error");
            parents.removeClass("correct");
            updateAnswerButtonState();
        }
        else {
            event.preventDefault();
            event.stopPropagation();
            return false;
        }
       
    });

    $("#checkBtn").click(function () {
        onAnswerBtn();
    });

    $("#results").css("visibility", "hidden");

    var answers = $("input.answerInput");
    for (var i = 0; i < answers.length; i++) {
        var answer = answers[i];
        var idParts = answer.id.split('_');
        var testIndex = parseInt(idParts[0].substr(6));
        var answerIndex = parseInt(idParts[1]);
        var answerData = theTest.tests[testIndex].answers[answerIndex];
        answerData.ui = answer;
        if (answerData.isCorrect == undefined)
            answerData.isCorrect = false;
    }
    var texteditanswers = $("input.answerTextedit");
    for (var i = 0; i < texteditanswers.length; i++)
    {
        var textedit = texteditanswers[i];
        var testIndex = parseInt(textedit.id.substr(6));
        theTest.tests[testIndex].texteditData.ui = textedit;
        $(textedit).bind("input", function (param) {
            updateAnswerButtonState();
            $(param.target).parents('div').removeClass("error");
            $(param.target).parents('div').removeClass("correct");
        });
    }
    var selects = $("select.answerSelect");
    for (var i = 0; i < selects.length; i++)
    {
        var select = selects[i];
        var testIndex = parseInt(select.id.substr(7));
        theTest.tests[testIndex].ui = select;
    }
}

function onAnswerBtn()
{
    var btn = $("#checkBtn");
    if (btn.hasClass("checkButtonDisabled"))
        return;
    state.attemptsLeft--;
    checkTest();

    showHints();

    updateAnswerButtonState();
}

function createTest(testData, index, number)
{
    var testHtml = String.format('<div class="test"><div><span class="theNumber">{0}</span> {1}</div><div class="answers">', number, testData.question);

    if (testData.type == "textedit")
    {
        var label = "";
        if (testData.texteditData.label)
            label = String.format('<span class="textEditLabel">{0}</span>', testData.texteditData.label);
        testHtml += String.format('<div class="answer"><input class="answerTextedit" type="text" name="textedit{0}" id="answer{0}">{1}</div>', index, label);
    }
    else if (testData.type == "select")
    {
        testHtml += String.format('<select class="answer answerSelect" id="answer_{0}">', index);
        for (var i = 0; i < testData.answers.length; i++)
        {
            var answer = testData.answers[i];
            testHtml += String.format('<option id="answer{0}_{1}">{2}</div>', index, i, answer.label, testData.type);
        }
        testHtml += '</select>';
    }
    else
    {
        for (var i = 0; i < testData.answers.length; i++) {
            var answer = testData.answers[i];
            testHtml += String.format('<div class="answer"><input class="magic-{3} answerInput" type="{3}" name="{3}{0}" id="answer{0}_{1}"><label for="answer{0}_{1}">{2}</label></div>', index, i, answer.label, testData.type);
        }
    }
    testHtml += '</div></div>';
    return testHtml;
}

function isAllTestsHasAnswer()
{
    for (var testIndex = 0; testIndex < theTest.tests.length; testIndex++)
    {
        if (theTest.tests[testIndex].type == "select")
        {
            continue;
        }
        else if (theTest.tests[testIndex].type == "textedit")
        {
            var ui = theTest.tests[testIndex].texteditData.ui;
            if (ui.value.length == 0)
                return false;
            continue;
        }
        else
        {
            var answers = theTest.tests[testIndex].answers;
            var isfound = false;
            for (var answerIndex = 0; answerIndex < answers.length; answerIndex++) {
                var obj = answers[answerIndex];
                if (obj.ui.checked) {
                    isfound = true;
                    break;
                }
            }
            if (!isfound) {
                return false;
            }
        }
    }
    return true;
}

function updateAnswerButtonState()
{
    var btn = $("#checkBtn");
    var isEditable = state.attemptsLeft > 0 && !state.isTestCompleted;
    var texteditanswers = $("input.answerTextedit,select");
    if (isEditable)
    {
        texteditanswers.removeAttr('disabled');
    }
    else
    {
        texteditanswers.attr('disabled', isEditable ? "false" : "true");
    }
    var isEnabled = isEditable && isAllTestsHasAnswer();

    if (isEnabled)
    {
        btn.removeClass("checkButtonDisabled");
        btn.addClass("checkButtonEnabled");
    }
    else
    {
        btn.removeClass("checkButtonEnabled");
        btn.addClass("checkButtonDisabled");
    }
}

function checkTest() {
    var res = check();
    if (res == "right") {
        state.isTestCompleted = true;
    }
    else {
        state.isTestCompleted = false;
    }
}

function check()
{
    var result = true;

    for (var testIndex = 0; testIndex < theTest.tests.length; testIndex++)
    {
        if (theTest.tests[testIndex].type == "textedit")
        {
            var data = theTest.tests[testIndex].texteditData;
            if (!checkTextEdit(data))
            {
                return 'wrong';
            }
        }
        else if (theTest.tests[testIndex].type == "select")
        {
            var answers = theTest.tests[testIndex].answers;
            var correctValue = "";
            for (var answerIndex = 0; answerIndex < answers.length; answerIndex++) {
                var answer = answers[answerIndex];
                if (answer.isCorrect)
                {
                    correctValue = answer.label;
                    break;
                }
            }
            if (theTest.tests[testIndex].ui.value != correctValue)
                return 'wrong';
        }
        else
        {
            var answers = theTest.tests[testIndex].answers;
            for (var answerIndex = 0; answerIndex < answers.length; answerIndex++) {
                var answer = answers[answerIndex];
                if (answer.ui.checked != answer.isCorrect) {
                    return 'wrong';
                }
            }
        }
    }
    return 'right';
}

function checkTextEdit(dataObj) {
    var result = false;
    var answer = dataObj.ui.value;
    if (!dataObj.isCaseSensitive)
        answer = answer.toLowerCase();
    answer = answer.replace(/ /g, '');
    if (dataObj.correctValues) {
        for (var i = 0; i < dataObj.correctValues.length; i++) {
            var correctAnswer = dataObj.correctValues[i];
            if (!dataObj.isCaseSensitive)
                correctAnswer = correctAnswer.toLowerCase();
            correctAnswer = correctAnswer.replace(/ /g, '');
            if (correctAnswer == answer) {
                result = true;
                break;
            }
        }
    }
    if (!result && dataObj.correctRange) {
        answer = parseInt(answer);
        result = answer >= dataObj.correctRange.start && answer <= dataObj.correctRange.stop;
    }
    var jObject = $(dataObj.ui);
    if (!result)
    {
        jObject.addClass("error");
        jObject.parent().parent().parent().addClass("error");
    }
    return result;
}

function checkSelect(dataObj)
{
    var result = false;
    var answer = dataObj.ui.value;
    var answers = dataObj.answers;
    var correctValue = "";
    for (var answerIndex = 0; answerIndex < answers.length; answerIndex++) 
    {
        if (answers[answerIndex].isCorrect)
        {
            correctValue = answers[answerIndex].label;
            break;
        }
    }
    result = answer == correctValue;
    var jObject = $(dataObj.ui);
    if (!result) {
        jObject.addClass("error");
        jObject.parent().parent().addClass("error");
    }
    return result;
}

function getState()
{
    state.answers = new Array();
    for (var testIndex = 0; testIndex < theTest.tests.length; testIndex++) {
        var arr = new Array();
        if (theTest.tests[testIndex].type == "textedit")
        {
            arr.push(theTest.tests[testIndex].texteditData.ui.value);
        }
        else {
            var answers = theTest.tests[testIndex].answers;

            for (var answerIndex = 0; answerIndex < answers.length; answerIndex++) {
                var answer = answers[answerIndex];
                arr.push(answer.ui.checked);
            }
        }
        state.answers.push(arr);
    }
    return JSON.stringify(state);
}

function setState(string)
{
    if (string && string != "") {
        state = JSON.parse(string);
        for (var testIndex = 0; testIndex < theTest.tests.length; testIndex++) {
            if (theTest.tests[testIndex].type == "textedit") {
                theTest.tests[testIndex].texteditData.ui.value = state.answers[testIndex][0];
            }
            else {
                var answers = theTest.tests[testIndex].answers;
                for (var answerIndex = 0; answerIndex < answers.length; answerIndex++) {
                    var answer = answers[answerIndex];
                    $(answer.ui).attr('checked', state.answers[testIndex][answerIndex]);
                }
            }
        }
        hideHints();
        if (state.attemptsLeft < theTest.attempts)
            showHints();
        updateAnswerButtonState();
    }
}